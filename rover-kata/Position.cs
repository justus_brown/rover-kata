namespace rover_kata
{
    public class Position
    {
        public Coordinate Coordinate { get; set; }
        public char Direction { get; set; }

        public Position(Coordinate coordinate, char direction)
        {
            Coordinate = coordinate;
            Direction = direction;
        }
    }
}
