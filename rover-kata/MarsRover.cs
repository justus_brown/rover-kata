using System;
using System.Collections.Generic;

namespace rover_kata
{
    public class MarsRover
    {
        public Position Position { get; set; }

        public MarsRover(Grid grid)
        {
            Position = new Position(new Coordinate(0, 0), 'N');
        }

        public string execute(string command)
        {
            for (int i = 0; i < command.Length; i++)
            {
                var singleCommand = char.ToUpper(command[i]);
                if (singleCommand == 'M')
                { 
                    move();
                }
                if (singleCommand == 'L' || singleCommand == 'R')
                {
                    turn(singleCommand);
                }
            }

            var result = $"{Position.Coordinate.X}:{Position.Coordinate.Y}:{Position.Direction}";
            return result ;
        }

        private List<char> directions = new() {'N', 'E', 'S', 'W'};

        private void turn(char turn)
        {
            var currentDirectionIndex = directions.FindIndex(x => x == Position.Direction);
            
            if (turn == 'R')
            {
                currentDirectionIndex++;

                if (currentDirectionIndex >= directions.Count)
                {
                    currentDirectionIndex = 0;
                }
            }
            else if (turn == 'L')
            {
                currentDirectionIndex--;

                if (currentDirectionIndex < 0)
                {
                    currentDirectionIndex = directions.Count - 1;
                }
            }
            
            Position.Direction = directions[currentDirectionIndex];
        }

        private void move()
        {
            switch (Position.Direction)
            {
                case 'N':
                    Position.Coordinate.Y++;
                    if (Position.Coordinate.Y >= 10)
                    {
                        Position.Coordinate.Y = 0;
                    }
                    break;
                case 'S':
                    Position.Coordinate.Y--;
                    if (Position.Coordinate.Y < 0)
                    {
                        Position.Coordinate.Y = 9;
                    }
                    break;
                case 'E':
                    Position.Coordinate.X++;
                    if (Position.Coordinate.X >= 10)
                    {
                        Position.Coordinate.X = 0;
                    }
                    break;
                case 'W':
                    Position.Coordinate.X--;
                    if (Position.Coordinate.X < 0)
                    {
                        Position.Coordinate.X = 9;
                    }
                    break;
            }
        }
    }
}
