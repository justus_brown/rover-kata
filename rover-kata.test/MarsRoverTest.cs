using NUnit.Framework;

namespace rover_kata.test
{
    public class MarsRoverTests
    {
        //[SetUp]
        //public void Setup()
        //{
        //}

        [Test]
        public void Test1()
        {
            var rover = new MarsRover(new Grid());
            var path = rover.execute("M");
            
            Assert.IsTrue(path == "0:1:N");
        }
        
        [Test]
        public void Test2()
        {
            var rover = new MarsRover(new Grid());
            var path = rover.execute("RM");
            
            Assert.IsTrue(path == "1:0:E");
        }
        
        [Test]
        public void Test3()
        {
            var rover = new MarsRover(new Grid());
            var path = rover.execute("LM");
            
            Assert.IsTrue(path == "9:0:W");
        }
        
        [Test]
        public void Test4()
        {
            var rover = new MarsRover(new Grid());
            var path = rover.execute("MMMMMMMMMM");
            
            Assert.IsTrue(path == "0:0:N");
        }
        
        [Test]
        public void Test5()
        {
            var rover = new MarsRover(new Grid());
            var path = rover.execute("RMMMMMMMMMM");
            
            Assert.IsTrue(path == "0:0:E");
        }
        
        [Test]
        public void Test6()
        {
            var rover = new MarsRover(new Grid());
            var path = rover.execute("RRM");
            
            Assert.IsTrue(path == "0:9:S");
        }
        
        [Test]
        public void Test7()
        {
            var rover = new MarsRover(new Grid());
            var path = rover.execute("RMMRMMLMR");
            
            Assert.IsTrue(path == "3:8:S");
        }
        
        [Test]
        public void Test8()
        {
            var rover = new MarsRover(new Grid());
            var path = rover.execute("RRMM");
            
            Assert.IsTrue(path == "0:8:S");
        }
    }
}
